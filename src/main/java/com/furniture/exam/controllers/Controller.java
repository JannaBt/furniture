package com.furniture.exam.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.furniture.exam.model.Furniture;
import com.furniture.exam.services.FurnitureService;

@RestController
@RequestMapping("furniture")
public class Controller {

	@Autowired
	private FurnitureService service;
	
	@PostMapping
	public String max(@RequestBody List<Furniture> furnitures) {
		return service.getMax(furnitures);
	}
	

}
