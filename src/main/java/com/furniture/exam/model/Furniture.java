package com.furniture.exam.model;

import lombok.*;

@AllArgsConstructor
@Data
public class Furniture {

	private String type;
	protected final Integer wood;
	protected final Integer glass;
	protected final Integer plastic;

}
