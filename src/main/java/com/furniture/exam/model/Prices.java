package com.furniture.exam.model;

public class Prices {
	
	public static final Integer WOOD_PRICE = 9;
	public static final Integer GLASS_PRICE = 11;
	public static final Integer PLASTIC_PRICE = 4;

}
