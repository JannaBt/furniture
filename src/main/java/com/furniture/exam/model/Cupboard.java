package com.furniture.exam.model;

import lombok.AllArgsConstructor;
import lombok.Data;


public class Cupboard extends Furniture implements FurnitureDAO<Cupboard> {

	public Cupboard(String type, Integer wood, Integer glass, Integer plastic) {
		super(type, wood, glass, plastic);
	}

	public Integer total() {
		if (wood == null) {
			return 0;
		}

		return wood * Prices.WOOD_PRICE;
	}

}
