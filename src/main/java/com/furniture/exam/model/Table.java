package com.furniture.exam.model;

public class Table extends Furniture implements FurnitureDAO<Table> {

	public Table(String type, Integer wood, Integer glass, Integer plastic) {
		super(type, wood, glass, plastic);
	}

	public Integer total() {

		if (wood == null || glass == null || plastic == null) {
			return 0;
		}

		return (wood * Prices.WOOD_PRICE) + (glass * Prices.GLASS_PRICE) + (plastic * Prices.PLASTIC_PRICE);
	}

}
