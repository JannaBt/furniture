package com.furniture.exam.model;

public class Chair extends Furniture implements FurnitureDAO<Chair>{

	public Chair(String type, Integer wood, Integer glass, Integer plastic) {
		super(type, wood, glass, plastic);
	}

	public Integer total() {

		if (wood == null || glass == null) {
			return 0;
		}

		return (wood * Prices.WOOD_PRICE) + (glass * Prices.GLASS_PRICE);
	}

}
