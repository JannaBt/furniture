package com.furniture.exam.services;

import java.util.ArrayList;
import java.util.List;

import com.furniture.exam.model.Chair;
import com.furniture.exam.model.Cupboard;
import com.furniture.exam.model.Furniture;
import com.furniture.exam.model.Table;

public class FurnitureServiceImpl implements FurnitureService {

	@Override
	public String getMax(List<Furniture> furnitures) {
		
		Integer sumTables = 0;
		Integer sumChairs = 0;
		Integer sumCupboards = 0;

		for (Furniture furniture : furnitures) {
			if (furniture.getType().equals("table") && furniture.getWood() != null && furniture.getGlass() != null
					&& furniture.getPlastic() != null) {
				Table table = new Table("table", furniture.getWood(), furniture.getGlass(), furniture.getPlastic());
				sumTables = table.total();
			} else if (furniture.getType().equals("chair") && furniture.getWood() != null
					&& furniture.getGlass() != null) {
				Chair chair = new Chair("chair", furniture.getWood(), furniture.getGlass(), 0);
				sumChairs += chair.total();
			} else if (furniture.getType().equals("cupboard") && furniture.getWood() != null) {
				Cupboard cupboard = new Cupboard("cupboard", furniture.getWood(), 0, 0);
				sumCupboards += cupboard.total();
			}
		}

		if (sumTables > sumChairs && sumTables > sumCupboards) {
			return "The furniture with highest price are the TABLES, with price of :" + sumCupboards;
		} else if (sumChairs > sumCupboards && sumChairs > sumTables) {
			return "The furniture with highest price are the CHAIRS, with price of :" + sumChairs;
		} else {
			return "The furniture with highest price are the CUPBOARDS, with price of :" + sumTables;
		}

	}

}
